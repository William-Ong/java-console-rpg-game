/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import runescape.model.Shop;

public class LoadByCSV implements FileLoader 
{
    /**
     * Function: load
     * Assertion: This method loads by taking in a csv formatted file
     *            to load the contents into the shop object.
     * @param file the file to be read
     * @param itemFactory the item factory to produce items
     * @param shop the shop to be changed
     * @return filled/appended shop contents
     * @throws IOException
     */
    @Override
    public Shop load(String file, ItemFactory itemFactory, Shop shop) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(file));

        String line = reader.readLine();
        while(line != null)
        {
            try
            {
                String[] itemArr = line.split(", ");
                shop.addItem(itemFactory.makeItem(itemArr));   
            }
            catch(NumberFormatException e)
            {
                reader.close();
                throw new IOException("Corrupted values detected in file content");
            }
            line = reader.readLine();
        }
        reader.close();

        return shop;
    }
    
}