/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

import runescape.model.Enemy;
import runescape.model.Item;
import runescape.model.Player;
import runescape.view.UserInterface;

public class BattleEngine 
{
    private Player player;
    private UserInterface ui;
    private Enemy enemy;
    private EntityFactory entityFactory;
    private double slimeProb, goblinProb, ogreProb, dragonProb;
    private boolean firstEncounter;

    /**
     * Function: Alternate Constructor of BattleEngine
     * Assertion: Creates an instance of the battle controller, this class is responsible for the battle
     *            system of the program.
     * @param player the player
     * @param ui the user interface
     * @param entityFactory the entity factory that creates enemy/player
     */
    public BattleEngine(Player player, UserInterface ui, EntityFactory entityFactory)
    {
        // Dependency injection
        this.player = player;
        this.ui = ui;
        this.entityFactory = entityFactory;
        
        // Enemy is set to null as it is a new battle field
        enemy = null;

        // Enemy spawn rate probability initialization
        slimeProb = 0.50;
        goblinProb = 0.30;
        ogreProb = 0.20;
        dragonProb = 0.0;

        // Controls the ASCII art display and the arrival messages when encountering the enemy
        firstEncounter = false;
    }

    /**
     * Function: battle
     * Assertion: The main battle function, controls the following:
     *              - Enemy spawn rate
     *              - Enemy's action
     *              - Player's action
     *              - Player's potion usage
     *            The function is ran in an infinite loop that will only exit if:
     *              - The player dies
     *              - The dragon has been slain
     *              - The player retreats
     */
    public void battle()
    {
        boolean finished = false;
        double enemySpawnRate = 0.0;
        ui.display("===========================\nBattle Phase\n===========================");
        
        /**
         * This condition is triggered when the player has retreated and decided to return to the battle
         * field to get more gold from enemies. The same enemy that the player ran from will greet them
         * once again, therefore display the enemy's ASCII and their arrival message once again.
         */
        if(enemy != null)
        {
            ui.display(enemy.asciiArt());
            ui.display("\n[The " + enemy.getName() + " approaches...]");
            firstEncounter = true;
        }

        // While the player is not dead, player didn't retreat or the dragon hasn't been slain, keep looping.
        while(!finished)
        {
            try
            {
                /**
                 * If there are currently no enemies existing in the battle controller, create one by their respective chances.
                 * Invoke a call to Math.random() and use that value to spawn an enemy by their spawn rate using a factory.
                 */
                if(enemy == null)
                {
                    enemySpawnRate = Math.random();
                    enemy = entityFactory.makeEnemy(enemySpawnRate < dragonProb ? "Dragon" : 
                                                    enemySpawnRate > dragonProb && enemySpawnRate < ogreProb ? "Ogre" :
                                                    enemySpawnRate > ogreProb && enemySpawnRate < goblinProb ? "Goblin" : 
                                                    enemySpawnRate > goblinProb && enemySpawnRate < slimeProb ? "Slime" : "");
                }
                /**
                 * Else if there is currently an enemy that is valid, begin the battle between the player and the enemy
                 */
                else if(enemy != null)
                {
                    /**
                     * If the player has not encountered the new enemy before, display the ASCII art and their arrival message,
                     * the boolean "firstEncounter" will be reset to false everytime the player retreats or defeats the enemy.
                     */
                    if(!firstEncounter)
                    {
                        ui.display(enemy.asciiArt());
                        ui.display("\n[The " + enemy.getName() + " approaches...]");
                        firstEncounter = true;
                    }

                    // The player's turn
                    ui.display("\n[Player's Turn]");

                    // Invoke a call to the user interface function in view to bring up a menu for the player to decide their actions.
                    int choice = ui.playerTurn(player);
                    
                    switch(choice)
                    {
                        /**
                         * If the player decides to attack, invoke a call to doAttack(), which will return an integer representing
                         * the damage that the player will do according to their weapon's minimum/maximum damage that can be dealt.
                         */
                        case 1:
                            int player_dmg = player.doAttack();
                            ui.display(player.toString());
                            ui.displayDefence(enemy, enemy.doDefense(player_dmg));
                            ui.displayDamageDone(player, enemy, player_dmg);
                            ui.displayHP(enemy);
                            break;
                        /**
                         * If the player decides to use a potion, they must know that checking their inventory will cost a turn,
                         * therefore, if they couldn't decide what potion to use, the enemy will have the advantage of attacking on
                         * their own turn.
                         */
                        case 2:
                            // If the player has no items inside their inventory, display a message to the player
                            if(player.isEmpty())
                            {
                                ui.display("[Empty inventory!]");
                            }
                            // If there are items inside the player's inventory
                            else
                            {
                                // Display all the items that the player has in their inventory
                                ui.display(player.checkInventory());

                                // Invoke a call to the user interface to retreive an option inputted by the player, given a prompt
                                int get_option = ui.intInput("(" + player.getInventorySize() + ") Exit\nSelect an item to use: ");

                                // If the option that the player inputted is within range, the item selected will be used in battle
                                if(get_option >= 0 && get_option < player.getInventorySize())
                                {
                                    // The player proceeds to check the item that they selected
                                    Item item = player.checkItem(get_option);
                                    // If the selected item is a potion, use it
                                    if(item.itemType() == 'P')
                                    {
                                        // Removes the item from the player's inventory
                                        item = player.getItemFromInventory(get_option);

                                        int use_option = ui.intInput("[Use on?]\n(1) " + player.getName() + "\n(2) " + enemy.getName() + "\n(3) Exit\nEnter an option: ");
                                        switch(use_option)
                                        {
                                            /**
                                             * To make the battle as versatile as possible, the player may choose to use the potion on whoever
                                             * they want, including using healing potion on the enemy if they choose to. Case 1 is if the potion
                                             * is used on the player themselves, case 2 is if the player uses the potion on the enemy. Note: If 
                                             * the player selects an option other than case 1 or case 2, it will signify that they've lost 
                                             * their turn and failed to decide, therefore, the enemy is allowed their turn.
                                             */
                                            case 1:
                                                ui.display("[Used " + item.getName() + " on " + player.getName() + "!]");
                                                item.useOn(player);
                                                ui.displayHP(player);
                                                break;
                                            case 2:
                                                ui.display("[Used " + item.getName() + " on " + enemy.getName() + "!]");
                                                item.useOn(enemy);
                                                ui.displayHP(enemy);
                                                break;
                                            default:
                                                ui.display("[You've failed to decide, lost turn!]");
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        ui.display("[You can't use anything other than potion!]");
                                    }
                                }
                                else
                                {
                                    ui.display("[Invalid selection! Lost turn.]");
                                }
                            }
                            break;
                        /**
                         * Ends the battle phase, set "finished" to true which allows the player to retreat but doing this
                         * will give the enemy their own turn. If the player dies in the retreating process, they will lose
                         * all progress.
                         */
                        case 4:
                            finished = true;
                            ui.display("[Attempts to retreat...]");
                            break;
                        default:
                            break;
                    }
        
                    /**
                     * If the enemy is still alive, they will be allows to execute their own action which might be
                     * a normal attack or a special ability, note that each enemy has their own unique special ability.
                     */
                    if(enemy.alive())
                    {
                        // Enemy's turn
                        ui.display("\n[Enemy's Turn]");
                        // Invoke a call to retrieve the enemy's damage which can be altered by their special abilities
                        int enemy_dmg = enemy.doAttack();
                        // The toString() method in each enemy signifies the attack that they've used, e.g. default attack or special abilities
                        ui.display(enemy.toString());
                        ui.displayDefence(player, player.doDefense(enemy_dmg));
                        ui.displayDamageDone(enemy, player, enemy_dmg);
                        ui.displayHP(player);
                        // If the player is dead
                        if(!player.alive())
                        {
                            // The battle phase has finished, the player has lost
                            finished = true;
                            ui.display("[" + player.getName() + " is kil. Game over. Returning to main menu...]\n");
                        }
                    }
                    else
                    {
                        ui.display("[You have defeated " + enemy.getName() + "!]\n");
                        player.receiveGold(enemy.getGold());
                        // Resting will regain some hitpoints for the player, will occur after every battle
                        player.rest();
                        ui.display(player.asciiArt());
                        ui.display("[" + player.getName() + " rested.]");
                        ui.displayHP(player);
                        ui.display("[Received " + enemy.getGold() + " gold!]");
                        ui.display("[Player's Gold Pouch: " + player.getGold() + "]");
                        // If the enemy slain was the dragon, the player has won the game and will be returned back to the main menu
                        if(enemy.getName().equalsIgnoreCase("dragon"))
                        {
                            finished = true;
                            ui.display("\n[The dragon has been defeated! You won! Returning to main menu...]\n");
                            ui.display(player.attribute());
                            // Reset all enemy's spawn rate
                            resetSpawnRate();
                        }
                        // Else if the enemy slain was a non-dragon, change spawn rate for all non-dragon enemies
                        else
                        {
                            changeSpawnRate();
                        } 

                        // Setting enemy to null will enable the first condition to be met so it creates another enemy
                        enemy = null; 
                        // The known enemy is defeated, setting this to false allows the new enemy to display their presence
                        firstEncounter = false;
                    }
                }
            }
            catch(NumberFormatException e)
            {
                ui.display("[Error in user input: " + e.getMessage() + ". Use integer options only!]");
            }
        }
    }

    /**
     * Function: resetSpawnRate
     * Assertion: Reset all enemies spawn rate to default
     */
    private void resetSpawnRate()
    {
        slimeProb = 0.50;
        goblinProb = 0.30;
        ogreProb = 0.20;
        dragonProb = 0.0;
    }

    /**
     * Function: changeSpawnRate
     * Assertion: Decrease the non-dragon spawn rate and increase the dragon's spawn rate
     */
    private void changeSpawnRate()
    {
        slimeProb -= 0.05;
        goblinProb -= 0.05;
        ogreProb -= 0.05;
        dragonProb += 0.15;
    }
}