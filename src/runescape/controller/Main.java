/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

import runescape.view.UserInterface;

public class Main 
{
    public static void main(String[] args)
    {
        ItemFactory itemFactory = new ItemFactory();
        EntityFactory entityFactory = new EntityFactory();
        LoaderFactory loaderFactory = new LoaderFactory();

        UserInterface ui = new UserInterface(loaderFactory);

        GameEngine engine = new GameEngine(ui, itemFactory, entityFactory);
        try
        {
            engine.setup();
        }
        catch(LoaderException e)
        {
            ui.display("[Loader error: " + e.getMessage() + "]");
        }
    }
}