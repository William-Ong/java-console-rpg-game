/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

import runescape.model.*;

public class EntityFactory 
{
    /**
     * Function: makePlayer
     * Assertion: A factory method that creates a new player object, given a name
     * @param name the name of the entity
     * @return the new Player object, with default parameters
     */
    public Player makePlayer(String name)
    {
        // If the player left the name field empty, it will be set to default "Nobody"
        return new Player(name.equals("") ? "Nobody" : name);
    }

    /**
     * Function: makeEnemy
     * Assertion: A factory method that creates a new enemy object, given a name
     * @param name the name of the entity
     * @return the new Enemy object, with default parameters
     */
    public Enemy makeEnemy(String name)
    {
        Enemy enemy = null;
        if(name.equalsIgnoreCase("slime"))
        {
            enemy = new Slime(name);
        }
        else if(name.equalsIgnoreCase("goblin"))
        {
            enemy = new Goblin(name);
        }
        else if(name.equalsIgnoreCase("ogre"))
        {
            enemy = new Ogre(name);
        }
        else if(name.equalsIgnoreCase("dragon"))
        {
            enemy = new Dragon(name);
        }
        return enemy;
    }
}