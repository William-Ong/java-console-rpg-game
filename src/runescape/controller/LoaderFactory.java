/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

import java.util.HashMap;
import java.util.Map;

public class LoaderFactory 
{
    private Map<Integer, FileLoader> loaders;

    /**
     * Function: Default Constructor of LoaderFactory
     * Assertion: Creates a factory that makes different file
     *            loading algorithms to load files.
     */
    public LoaderFactory()
    {
        loaders = new HashMap<>();
        loaders.put(0, new LoadByCSV());
    }

    /**
     * Function: makeLoader
     * Assertion: Depending on the file format, make a loader based
     *            on the file and load the contents.
     * @param file the file to be loaded
     * @return the loader class
     */
    public FileLoader makeLoader(String file)
    {
        FileLoader loader = null;
        if(file.endsWith(".csv"))
        {
            loader = loaders.get(0);
        }
        else
        {
            throw new IllegalArgumentException("[Invalid file format! Only accept [.csv] format]");
        }
        return loader;
    }
}