/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import runescape.model.Item;
import runescape.model.Player;
import runescape.model.Shop;
import runescape.view.UserInterface;

public class ShopEngine 
{
    private UserInterface ui;
    private Shop shop;
    private ItemFactory itemFactory;
    private List<Item> shop_items;
    private List<String> enchantments;
    private List<Integer> enchantments_cost;
    private List<String> enchantments_name;
    private int shop_index, enchant_index;

    /**
     * Function: Alternate Constructor of ShopEngine
     * Assertion: Creates and initializes all methods/fields of the class
     * @param ui the user interface
     * @param shop the shop model class
     * @param itemFactory the item factory
     */
    public ShopEngine(UserInterface ui, Shop shop, ItemFactory itemFactory)
    {
        // Dependency Injection
        this.ui = ui;
        this.shop = shop;
        this.itemFactory = itemFactory;
        
        // Retrieve the list of shop items from the shop model class
        shop_items = shop.getList();

        // Used for shop list iteration
        shop_index = 0;

        // Create and initialize a list of string that is used to display the description of each enchantments
        enchantments = new ArrayList<>();
        enchantments.add("Name: Damage +2   | Cost: 5  | Effect: Adds 2 to attack damage.");
        enchantments.add("Name: Damage +5   | Cost: 10 | Effect: Adds 5 to attack damage.");
        enchantments.add("Name: Fire Damage | Cost: 20 | Effect: Adds between 5-10 to attack damage.");
        enchantments.add("Name: Power-Up    | Cost: 10 | Effect: Multiplies attack damage by 1.1.");
        enchantments.add("Exit");

        // Create and initialize a list of integers that represents the cost of each enchantment
        enchantments_cost = new ArrayList<>();
        enchantments_cost.add(5);
        enchantments_cost.add(10);
        enchantments_cost.add(20);
        enchantments_cost.add(10);

        // Create and initialize a list of string that represents the name of the enchantment
        enchantments_name = new ArrayList<>();
        enchantments_name.add("Damage +2");
        enchantments_name.add("Damage +5");
        enchantments_name.add("Fire Damage");
        enchantments_name.add("Power-Up");

        //Used for enchantment list iteration
        enchant_index = 0;
    }

    /**
     * Function: openShop
     * Assertion: Display the shop menu and takes in user option to buy items
     * @param player the Player object
     * @throws LoaderException
     */
    public void openShop(Player player) throws LoaderException
    {
        int shop_choice = 0;
        if(player != null)
        {
            shop_choice = ui.showShopOption();
            switch(shop_choice)
            {
                case 1:
                    displayShop();
                    ui.display(player.attribute());
                    ui.display("To buy enchantments, enter [-1]");
                    selectItemToBuy(ui.intInput("Select an item to buy: "), player);
                    break;
                case 2:
                    selectItemToSell(ui.intInput(player.checkInventory() + "\n(" + player.getInventorySize() + ") Exit\nSelect an item to sell: "), player);
                    break;
                case 3:
                    String file = ui.strInput("Enter a file name: ");
                    loadNewShopContents(file);
                    break;
                default:
                    ui.display("[Invalid option! Try again.]");
                    break;
            }
        }
        else
        {
            ui.display("[Warning: Make a character first before going into the shop!]");
        }
    }

    /**
     * Function: loadNewShopContents
     * Assertion: This section allows the player to load more items mid-game that 
     *            gives the player a change of pace whenever they feel like it.
     * @param file the file to be
     * @throws LoaderException
     */
    private void loadNewShopContents(String file) throws LoaderException
    {
        FileLoader loader = null;
        try
        {
            loader = ui.getFileLoader(file);
            if(loader != null)
            {
                shop = loader.load(file, itemFactory, this.shop);
                // Update shop with more contents
                shop_items = shop.getList();
                ui.display("[Shop contents loaded! Successfully updated.]");
            }
        }
        catch(LoaderException e)
        {
            ui.display(e.getMessage());
        }
        catch(IllegalArgumentException e)
        {
            ui.display(e.getMessage());
        }
        catch(IOException e)
        {
            throw new LoaderException(e.getMessage(), e);
        }
    }

    /**
     * Function: getStartUpWeapon
     * Assertion: Gives the player the cheapest available weapon from the shop 
     * @return the cheapest weapon
     */
    public Item getStartUpWeapon()
    {
        Item startUpWeapon = null;
        int min = Integer.MAX_VALUE;
        for(Item item : shop_items)
        {
            if(item.itemType() == 'W' && item.getCost() <= min)
            {  
                startUpWeapon = item;
                min = item.getCost();
            }
        }
        return startUpWeapon;
    }

    /**
     * Function: getStartUpArmor
     * Assertion: Gives the player the cheapest avaialble armor from the shop
     * @return the cheapest armor
     */
    public Item getStartUpArmor()
    {
        Item startUpArmor = null;
        int min = Integer.MAX_VALUE;
        for(Item item : shop_items)
        {
            if(item.itemType() == 'A' && item.getCost() <= min)
            {  
                startUpArmor = item;
                min = item.getCost();
            }
        }
        return startUpArmor;
    }

    /**
     * Function: selectItemToBuy
     * Assertion: Given an option selection, it will get a reference of the item
     *            from the shop list that will be added into the player's inventory, 
     *            deducting the player's gold in the process.
     * @param buying_choice the player's buying choice
     * @param player the Player object
     */
    private void selectItemToBuy(int buying_choice, Player player)
    {
        Item weapon = null;
        // If the choice selected is within range and the player still has space in their inventory
        if(buying_choice >= 0 && buying_choice < getShopItemCount() && !player.checkInventorySpace())
        {
            Item gItem = buyItem(buying_choice);
            if(gItem.getCost() <= player.getGold())
            {
                ui.display("[Player Bought: " + gItem.getName() + "]");
                player.spendGold(gItem.getCost());
                player.addToInventory(gItem);
                ui.display("[Player's Gold Pouch: " + player.getGold() + "]");
            }
            else
            {
                ui.display("[Not enough gold!]");
            }
        }
        // If the player decides to enchant their weapons
        else if(buying_choice == -1)
        {
            displayEnchantments();
            weapon = player.getEquippedWeapon();
            buyEnchantments(weapon, ui.intInput("Select an enchantment to buy: "), player);
        }
        else if(player.checkInventorySpace())
        {
            ui.display("[Inventory full!]");
        }
        else if(buying_choice == getShopItemCount())
        {
            ui.display("[Exiting...]");
        }
        else
        {
            ui.invalidOption();
        }
    }

    /**
     * Function: selectItemToSell
     * Assertion: Given an option selection, it will remove an item from the
     *            user's inventory to be sold to the shop to gain some gold.
     * @param selling_choice the player's selling choice
     * @param player the Player object
     */
    private void selectItemToSell(int selling_choice, Player player)
    {
        if(selling_choice >= 0 && selling_choice < player.getInventorySize())
        {
            Item item = player.getItemFromInventory(selling_choice);
            int soldItemValue = sellItem(item);
            player.receiveGold(soldItemValue);
            ui.display("[Sold " + item.getName() + " for " + soldItemValue + " gold!]");
            ui.display("[Player's Gold Pouch: " + player.getGold() + "]");
        }
        else if(selling_choice == player.getInventorySize())
        {
            ui.display("[Exiting...]");
        }
        else
        {
            ui.invalidOption();
        }
    }

    /**
     * Function: buyEnchantments
     * Assertion: Given a weapon and an option selection, enchant the current weapon
     *            with the player selected enchantment.
     * @param weapon the weapon to be enchanted
     * @param buying_choice the player's buying choice
     * @param player the Player object
     * @throws NumberFormatException
     */
    private void buyEnchantments(Item weapon, int buying_choice, Player player) throws NumberFormatException
    {
        int equip_choice = 0;
        int enchantment_choice = 0;
        if(buying_choice >= 0 && buying_choice <= 3 && getEnchantmentCost(buying_choice) <= player.getGold())
        {
            enchantment_choice = ui.showEnchantmentOption();
            switch(enchantment_choice)
            {
                case 1:
                    enchantEquippedWeapon(weapon, buying_choice, player);
                    break;
                case 2:
                    enchantInventoryWeapon(equip_choice, buying_choice, player);
                    break;
                default:
                    ui.invalidOption();
                    break;
            }
        }
        else if(buying_choice < 0 || buying_choice > 4)
        {
            ui.invalidOption();
        }
        else if(buying_choice == 4)
        {
            ui.display("[Exiting...]");
        }
        else
        {
            ui.display("[Not enough gold!]");
        }
    }

    /**
     * Function: enchantEquippedWeapon
     * Assertion: Enchant the current equipped weapon on the player
     * @param weapon the weapon to be enchanted
     * @param buying_choice the player's buying choice
     * @param player the Player object
     */
    private void enchantEquippedWeapon(Item weapon, int buying_choice, Player player)
    {
        if(weapon != null)
        {
            Item enchantedWeapon = itemFactory.makeEnchantedWeapon(weapon, buying_choice);
            player.spendGold(getEnchantmentCost(buying_choice));
            player.equipEnchantedWeapon(enchantedWeapon);
            enchantedWeapon.useOn(player);
            ui.display("[Enchanted " + weapon.getName() + " with " + getEnchantmentName(buying_choice) + "]");
            ui.display("[Player's Gold Pouch: " + player.getGold() + "]");
        }
        else
        {
            ui.display("[Equip a weapon to enchant.]");
        }
    }

    /**
     * Function: enchantInventoryWeapon
     * Assertion: Enchant a weapon in the player's inventory
     * @param equip_choice the player's equip choice
     * @param buying_choice the player's buying choice
     * @param player the Player object
     */
    private void enchantInventoryWeapon(int equip_choice, int buying_choice, Player player)
    {
        Item weapon = null;

        if(player.isEmpty())
        {
            ui.display("[Empty inventory!]");
        }
        else
        {
            ui.display(player.checkInventory());
            equip_choice = ui.intInput("(" + player.getInventorySize() + ") Exit\nSelect a weapon to enchant: ");
            if(equip_choice >= 0 && equip_choice < player.getInventorySize())
            {
                if(player.checkItem(equip_choice).itemType() == 'W')
                {
                    player.spendGold(getEnchantmentCost(buying_choice));
                    ui.display("[Selected " + player.checkItem(equip_choice).getName() + " to enchant.]");
                    weapon = player.getItemFromInventory(equip_choice);
                    player.addToInventory(itemFactory.makeEnchantedWeapon(weapon, buying_choice));
                    ui.display("[Enchanted " + weapon.getName() + " with " + getEnchantmentName(buying_choice) + "]");
                    ui.display("[Player's Gold Pouch: " + player.getGold() + "]");
                }
                else
                {
                    ui.display("[You can't enchant anything other than weapon!]");
                }
            }
            else
            {
                ui.invalidOption();
            }
        }
    }

    
    /**
     * Function: buyItem
     * Assertion: Retrieves an item using a specific index from the shop list
     * @param index the index to be used to retrieve the item
     * @return the item that is to be bought by the player, it is returned by reference
     */
    private Item buyItem(int index)
    {
        return shop.getItem(index);
    }

    /**
     * Function: sellItem
     * Assertion: When the player sells an item, it will be sold 50% of its original value
     * @param item the item to be sold
     * @return the value of the sold item
     */
    private int sellItem(Item item)
    {
        int value = item.getCost()/2;
        return value;
    }
    
    /**
     * Function: getShopItemCount
     * Assertion: Get the total number of items that the shop has
     * @return the total number of items inside the shop list
     */
    private int getShopItemCount()
    {
        return shop.getSize();
    }

    /**
     * Function: getEnchantmentCost
     * Assertion: Retrieves an enchantment cost from the list of cost
     * @param index the index to retrieve the cost
     * @return the cost of the enchantment
     */
    private int getEnchantmentCost(int index)
    {
        return enchantments_cost.get(index);
    }

    /**
     * Function: getEnchantmentName
     * Assertion: Retrieves the enchantment's name from the list of name
     * @param index the index to retreive the name
     * @return the name of enchantment
     */
    private String getEnchantmentName(int index)
    {
        return enchantments_name.get(index);
    }

    /**
     * Function: displayShop
     * Assertion: Display the shop items to the user, alongside with the option to exit the shop
     */
    private void displayShop()
    {
        shop_index = 0;
        ui.display("====================\nWelcome to the Shop!\n====================");
        for(Item item : shop_items)
        {
            ui.display("(" + shop_index + ") Item: " + item.getName() + " | Cost: " + item.getCost() + " | Type: " + item.getType());
            shop_index++;
        }
        ui.display("(" + shop_items.size() + ") Exit");
        ui.display("====================");
    }

    /**
     * Function: displayEnchantments
     * Assertion: Display the enchantment options to the user
     */
    private void displayEnchantments()
    {
        enchant_index = 0;
        ui.display("====================\nEnchantments\n====================");
        for(String enchantment : enchantments)
        {
            ui.display("(" + enchant_index + ") " + enchantment);
            enchant_index++;
        }
        ui.display("====================");
    }
}