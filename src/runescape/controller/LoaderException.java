/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

public class LoaderException extends Exception
{
    private static final long serialVersionUID = 4581774231408703463L;

    public LoaderException(String errorMessage, Throwable cause)
    {
        super(errorMessage, cause);
    }    
}