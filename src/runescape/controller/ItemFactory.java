/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

import runescape.model.*;

public class ItemFactory 
{
    /**
     * Function: makeItem
     * Assertion: A factory method that creates item using the given import
     * @param itemArr the string array that contains the item's information
     * @return a constructed Item object
     */
    public Item makeItem(String[] itemArr)
    {
        String itemType = itemArr[0];
        String name = itemArr[1];
        int minEffect = Integer.parseInt(itemArr[2]);
        int maxEffect = Integer.parseInt(itemArr[3]);
        int cost = Integer.parseInt(itemArr[4]);
        String misc = itemArr[5];
        String weaponType = "";
        // This condition is only unique to weapon where it has one more than the rest in terms of miscellaneous information
        if(itemArr.length == 7)
        {
            weaponType = itemArr[6];
        }

        Item item = null;
        switch(itemType.charAt(0))
        {
            case 'W': case 'w':
                item = new BaseWeapon(name, minEffect, maxEffect, cost, "Weapon", misc, weaponType);
                break;
            case 'A': case 'a':
                item = new Armor(name, minEffect, maxEffect, cost, "Armor", misc);
                break;
            case 'P': case 'p':
                item = new Potion(name, minEffect, maxEffect, cost, "Potion", misc.charAt(0));
                break;
            default:
                break;
        }

        return item;
    }

    /**
     * Function: makeEnchantedWeapon
     * Assertion: A factory method that takes in a weapon and returns an enchanted
     *            version of that same weapon.
     * @param weapon the weapon to be enchanted
     * @param choice the choice that the user made when purchasing enchantments
     * @return the newly enchant item object
     */
    public Item makeEnchantedWeapon(Item weapon, int choice)
    {
        switch(choice)
        {
            case 0:
                weapon = new DamageTwoEnchantment(weapon);
                break;
            case 1:
                weapon = new DamageFiveEnchantment(weapon);
                break;
            case 2:
                weapon = new FireEnchantment(weapon);
                break;
            case 3:
                weapon = new PowerEnchantment(weapon);
                break;
            default:
                break;
        }
        return weapon;
    }
}