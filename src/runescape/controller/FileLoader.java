/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

import java.io.IOException;

import runescape.model.Shop;

public interface FileLoader 
{
    public Shop load(String file, ItemFactory itemFactory, Shop shop) throws IOException;    
}