/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.controller;

import java.io.IOException;

import runescape.view.UserInterface;
import runescape.model.*;

public class GameEngine 
{
    private UserInterface ui;
    private ItemFactory itemFactory;
    private EntityFactory entityFactory;

    private ShopEngine se;
    private BattleEngine bEngine;
    private Player player;
    private boolean finished;

    /**
     * Function: Alternate Constructor of GameEngine
     * Assertion: Creates an instance of the main controller, this class controls the main menu aspect
     *            of the game. 
     * @param ui the user interface
     * @param itemFactory the item factory that creates type weapon, armor and potions
     * @param entityFactory the entity factory that creates player/enemies
     */
    public GameEngine(UserInterface ui, ItemFactory itemFactory, EntityFactory entityFactory)
    {
        // Dependency injection
        this.ui = ui;
        this.itemFactory = itemFactory;
        this.entityFactory = entityFactory;
        
        // Class field initialization
        se = null;
        bEngine = null;
        player = null;
        finished = false;
    }

    /**
     * Function: setup
     * Assertion: Retrieves the file name from the user input and attempts to read it.
     *            If file is valid, run the main menu.
     * @throws LoaderException
     */
    public void setup() throws LoaderException
    {
        FileLoader loader = null;
        String file = ui.strInput("Enter a file name: ");
        try
        {
            loader = ui.getFileLoader(file);
            if(loader != null)
            {
                Shop shop = loader.load(file, itemFactory, new Shop());
                gameMenu(shop);
            }
        }
        catch(LoaderException e)
        {
            ui.display(e.getMessage());
        }
        catch(IllegalArgumentException e)
        {
            ui.display(e.getMessage());
        }
        catch(IOException e)
        {
            throw new LoaderException(e.getMessage(), e);
        }
    }

    /**
     * Function: gameMenu
     * Assertion: Main menu of the game, controls the following:
     *              - Open the shop for the player to buy/sell items
     *              - Creates a new character/set a new name for the character
     *              - Wear equipments that are available in the inventory to the player
     *              - Start the battle to defeat the dragon
     *              - Exiting the program as a whole
     */
    public void gameMenu(Shop shop)
    {
        // Creates a new instance of the shop controller, allowing the player to buy and interact with the shop
        se = new ShopEngine(ui, shop, itemFactory);

        ui.display("===========================\nWelcome to Runescape!\n===========================");
        while(!finished)
        {
            try
            {
                /**
                 * If the player has return to the main menu because they have been defeated in battle, reset
                 * the player's progress and the battle progress.
                 */
                if(player != null && !player.alive())
                {
                    player = null;
                    bEngine = null;
                }

                // Invokes a call to user interface to show the main menu and allow the user to enter their option.
                int choice = ui.showMenu(player);
                switch(choice)
                {
                    case 1:
                        se.openShop(player);
                        break;
                    case 2:
                        characterCreation(ui.strInput((player == null) ? "Enter a name for your new character: " : "Enter a new name for your current character: "));
                        break;
                    case 3:
                        wearGear('W');
                        break;
                    case 4:
                        wearGear('A');
                        break;
                    case 5:
                        startBattle();
                        break;
                    case 6:
                        ui.display("...Closing game...\n[*]...Do not power off...[*]");
                        finished = true;
                        break;
                    default:
                        ui.display("[Invalid option! Try again.]");
                        break;
                }
            }
            catch(NumberFormatException e)
            {
                ui.display("[Error in user input: " + e.getMessage() + ". Use integer options only!]");
            }
            catch(LoaderException e)
            {
                ui.display("[Loader error: " + e.getMessage() + "]");
            }
        }
    }

    /**
     * Function: characterCreation
     * Assertion: Creates a Player object by importing a string which represents the name
     *            of the character, if there exist a player, change the name of the 
     *            current character instead
     * @param name the name of the character
     */
    public void characterCreation(String name)
    {
        if(player == null)
        {
            player = entityFactory.makePlayer(name);
            /**
             * New player will be given a set of the cheapest weapon and armor from
             * the shop for free and they will equipped to their respective slot.
             */
            player.equipWeapon(se.getStartUpWeapon());
            player.equipArmor(se.getStartUpArmor());
            ui.display(player.attribute());
        }
        else
        {
            player.setName(name);
            ui.display(player.attribute());
        }
    }

    /**
     * Function: wearGear
     * Assertion: Allows the player to equip a gear to their respective slot depending on the 
     *            equipment type. 
     * @param equipmentType the character which determines which slot to equip the items to
     * @throws NumberFormatException
     */
    public void wearGear(char equipmentType) throws NumberFormatException
    {
        if(player != null)
        {
            // Display the player's entire inventory
            ui.display(player.checkInventory());

            int equip_choice = ui.intInput("(" + player.getInventorySize() + ") Unequip " + (equipmentType == 'W' ? "weapon" : "armor") + 
                                           "\n(" + (player.getInventorySize() + 1) + ") Exit\nSelect " + (equipmentType == 'W' ? "a weapon" : "an armor") + " to equip: ");
            if(equip_choice >= 0 && equip_choice < player.getInventorySize())
            {
                // If the selected item conforms to the requested equipment type, then equip it
                if(player.checkItem(equip_choice).itemType() == equipmentType)
                {
                    Item gear = player.getItemFromInventory(equip_choice);
                    ui.display("[Equipped " + gear.getName() + " to " + (equipmentType == 'W' ? "weapon" : "armor") + " slot.]");
                    if(equipmentType == 'W')
                    {
                        player.equipWeapon(gear);
                        gear.useOn(player);
                    }
                    else if(equipmentType == 'A')
                    {
                        player.equipArmor(gear);
                        gear.useOn(player);
                    }
                }
                else
                {
                    ui.display("[You can't equip anything other than " + (equipmentType == 'W' ? "weapon" : "armor") + "!]");
                }
            }
            // This condition display an additional option for the player to unequip their current weapon/armor
            else if(equip_choice == player.getInventorySize())
            {
                Item unequippedItem = null;
                switch(equipmentType)
                {
                    case 'W':
                        unequippedItem = player.getEquippedWeapon();
                        if(unequippedItem != null)
                        {
                            player.unequipWeapon();
                            ui.display("[You have unequipped " + unequippedItem.getName() + "!]");
                        }
                        else
                        {
                            ui.display("[You have no weapon equipped!]");
                        }
                        break;
                    case 'A':
                        unequippedItem = player.getEquippedArmor();
                        if(unequippedItem != null)
                        {
                            player.unequipArmor();
                            ui.display("[You have unequipped " + unequippedItem.getName() + "!]");
                        }
                        else 
                        {
                            ui.display("[You have no armor equipped!]");
                        }
                        break;  
                    default:
                        break;
                }
            }
            else if(equip_choice == player.getInventorySize() + 1)
            {
                ui.display("[Exiting...]");
            }
            else
            {
                ui.invalidOption();
            }
        }
        else
        {
            ui.display("[Warning: Make a character!]");
        }
    }

    /**
     * Function: startBattle
     * Assertion: Starts the battle by creating an instance of the battle controller and then
     *            calling the battle() method to start the battle phase.
     */
    public void startBattle()
    {
        if(player != null && bEngine == null)
        {
            bEngine = new BattleEngine(player, ui, entityFactory);
        }
        else if(player == null)
        {
            ui.display("[Warning: Make a character first before going into battle!]");
        }

        // An additional check in case the user has returned to the main menu but wants to proceed in the battle
        if(bEngine != null)
        {
            bEngine.battle();
        } 
    }

}