/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

import java.util.ArrayList;
import java.util.List;

public class Player extends Entities
{
    private static final int MAX_INVENTORY = 15;
    private List<Item> inventory;
    private Item equippedWeapon;
    private Item equippedArmor;

    /**
     * Function: Alternate Constructor of Player
     * Assertion: This creates an instance of a Player object that 
     *            enables the user to be able to start playing the
     *            game itself.
     * @param name the name of the player
     */
    public Player(String name)
    {
        super(name, 30, 1, 1, 5, 5, 100);
        maxHealth = health;
        inventory = new ArrayList<>();
        equippedWeapon = null;
        equippedArmor = null;
    }

    /**
     * Function: doAttack
     * Assertion: Every entity has their own attack calculation, this will
     *            differ for player and enemy as all enemy has a chance to
     *            trigger their special ability while the player does not.
     *            Instead, the player relies on their equipped weapon to
     *            deal damage.
     * @return the damage to be dealt
     */
    @Override
    public int doAttack()
    {
        return (equippedWeapon != null ? equippedWeapon.effect() : getMinMaxRand(minDamage, maxDamage));
    }

    /**
     * Function: doDefense
     * Assertion: Every entity has their own defense calculation, this
     *            calculates the likelihood in which does the entity
     *            takes or absorbs the damage from an attack.
     * @param damage the damage to be adsorbed
     * @return the defense value calculated
     */
    @Override
    public int doDefense(int damage)
    {
        int defence = (equippedArmor != null ? equippedArmor.effect() : getMinMaxRand(minDamage, maxDamage));
        if(defence < damage)
        {
            health -= Math.max(0, damage - defence);
        }
        return defence;
    }
    
    /**
     * Function: toString
     * Assertion: Returns a string describing the player's attack
     * @return the attack description
     */
    @Override
    public String toString()
    {
        return "[" + name + " use their trusty " + equippedWeapon.getName() + " to attack!]";
    }

    /**
     * Function: asciiArt
     * Assertion: Display an ASCII art that describes the player resting
     * Reference: https://www.asciiart.eu/nature/camping
     * @return the ASCII art formatted to be printed
     */
    @Override
    public String asciiArt()
    {
        return
            "        (                 ,&&&.\n" +
            "        )                .,.&&\n" +
            "       (  (              \\=__/\n" +
            "           )             ,'-'.\n" +
            "     (    (  ,,      _.__|/ /|\n" +
            "      ) /\\ -((------((_|___/ |\n" +
            "    (  // | (`'      ((  `'--|\n" +
            "  _ -.;_/ \\\\--._      \\\\ \\-._/.\n" +
            " (_;-// | \\ \\-'.\\    <_,\\_\\`--'|\n" +
            " ( `.__ _  ___,')      <_,-'__,'\n" +
            "      `'(_ )_)(_)_)'";
    }

    /**
     * Function: rest
     * Assertion: After every enemy killed, the player is allowed
     *            to rest and recover some health, which will cap
     *            at their maximum health.
     */
    public void rest()
    {
        health = Math.min(maxHealth, (int)(health * 1.5));
    }

    /**
     * Function: equipWeapon
     * Assertion: Equips a weapon to the player weapon slot, if
     *            there is another weapon currently equipped, it
     *            will be swapped out and place the old weapon back
     *            into the inventory.
     * @param weapon the weapon to be equipped
     */
    public void equipWeapon(Item weapon)
    {
        Item usedWeapon = null;
        if(equippedWeapon == null)
        {
            equippedWeapon = weapon;
        }
        else
        {
            usedWeapon = equippedWeapon;
            equippedWeapon = weapon;
            addToInventory(usedWeapon);
        }
    }

    /**
     * Function: equipEnchantedWeapon
     * Assertion: Due to designs to equip normal weapon, this method
     *            makes sures that the enchantment process won't produce
     *            duplicated weapons.
     * @param enchantedWeapon
     */
    public void equipEnchantedWeapon(Item enchantedWeapon)
    {
        equippedWeapon = enchantedWeapon;
    }

    /**
     * Function: equipArmor
     * Assertion: Equips a armor to the player armor slot, if
     *            there is another armor currently equipped, it
     *            will be swapped out and place the old armor back
     *            into the inventory.
     * @param armor the armor to be equipped
     */
    public void equipArmor(Item armor)
    {
        Item usedArmor = null;
        if(equippedArmor == null)
        {
            equippedArmor = armor;
        }
        else
        {
            usedArmor = equippedArmor;
            equippedArmor = armor;
            addToInventory(usedArmor);
        }
    }

    /**
     * Function: unequipWeapon
     * Assertion: Unequips the currently equipped weapon and place
     *            back into the inventory
     */
    public void unequipWeapon()
    {
        addToInventory(equippedWeapon);
        equippedWeapon = null;
    }

    /**
     * Function: unequipArmor
     * Assertion: Unequips the currently equipped armor and place
     *            back into the inventory
     */
    public void unequipArmor()
    {
        addToInventory(equippedArmor);
        equippedArmor = null;
    }

    /**
     * Function: getEquippedWeapon
     * Assertion: Get the player's currently equipped weapon
     * @return the player's weapon
     */
    public Item getEquippedWeapon()
    {
        return equippedWeapon;
    }

    /**
     * Function: getEquippedArmor
     * Assertion: Get the player's currently equipped armor
     * @return the player's armor
     */
    public Item getEquippedArmor()
    {
        return equippedArmor;
    }

    /**
     * Function: addToInventory
     * Assertion: Adds item into the player's inventory
     * @param item the item to be added
     */
    public void addToInventory(Item item)
    {        
        inventory.add(item);
    }

    /**
     * Function: checkItem
     * Assertion: Gets a reference to an item in a specific
     *            position in the inventory using an index.
     * @param index the index to get the reference
     * @return the reference to the item
     */
    public Item checkItem(int index)
    {
        return inventory.get(index);
    }

    /**
     * Function: getItemFromInventory
     * Assertion: Removes the item object from the list
     *            entirely using an index and returns it.
     * @param index the index to get the object
     * @return the item object being returned
     */
    public Item getItemFromInventory(int index)
    {
        return inventory.remove(index);
    }

    /**
     * Function: checkInventorySpace
     * Assertion: Checks if the player's inventory is currently
     *            full.
     * @return a true/false if the inventory is full or not
     */
    public boolean checkInventorySpace()
    {
        return inventory.size() == MAX_INVENTORY;
    }

    /**
     * Function: isEmpty
     * Assertion: Checks if the player has any items in their inventory
     * @return a true/false if the inventory is empty or not
     */
    public boolean isEmpty()
    {
        return inventory.size() == 0;
    }

    /**
     * Function: getInventorySize
     * Assertion: Get the current inventory size
     * @return the inventory size
     */
    public int getInventorySize()
    {
        return inventory.size();
    }

    /**
     * Function: checkInventory
     * Assertion: Check the list of items that the
     *            player has in their inventory.
     * @return the string containing the inventory information
     */
    public String checkInventory()
    {
        String str = "\n[Player's Inventory]";
        int i = 0;
        for(Item item : inventory)
        {
            str += "\n(" + i + ")" + " Name: " + item.getName() + " | Type: " + item.getType();
            i++;
        }

        return str;
    }

    /**
     * Function: attribute
     * Assertion: Display the statistic of the player
     * @return the string containing all the information regarding the player
     */
    public String attribute()
    {
        return "[Player's statistics]\n|Name: " + name + 
                                    "\n|Health: " + health + 
                                    "\n|Gold: " + gold + 
                                    "\n|Weapon: " + ((equippedWeapon != null) ? equippedWeapon.getName() : "Empty") +
                                    "\n|Armor: " + ((equippedArmor != null) ? equippedArmor.getName() : "Empty") +
                                    "\n|Minimum Damage: " + ((equippedWeapon != null) ? equippedWeapon.getMinEffect() : 0) +
                                    "\n|Maximum Damage: " + ((equippedWeapon != null) ? equippedWeapon.getMaxEffect() : 0) +
                                    "\n|Minimum Defence: " + ((equippedArmor != null) ? equippedArmor.getMinEffect() : 0) +
                                    "\n|Maximum Defence: " + ((equippedArmor != null) ? equippedArmor.getMaxEffect() : 0) +
                                    "\n====================";
    }

    /**
     * Function: spendGold
     * Assertion: Calculates the amount of gold that the player has spent
     * @param cost the cost of the item
     */
    public void spendGold(int cost)
    {
        gold -= cost;
    }

    /**
     * Function: receiveGold
     * Assertion: Calculates the amount of gold that the player has received
     * @param award the gold that is awarded to the player
     */
    public void receiveGold(int award)
    {
        gold += award;
    }
}