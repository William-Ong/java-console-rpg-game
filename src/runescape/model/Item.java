/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

import java.util.Random;

public abstract class Item 
{
    protected String name, type;
    protected int cost, minEffect, maxEffect;
    protected Random rand;

    /**
     * Function: Default Constructor of Item
     * Assertion: This is required for the weapon decorator to work
     */
    public Item()
    {
        /**
         * INSTANTIATES ITEM
         */
    }

    /**
     * Function: Alternate Constructor of Item
     * Assertion: This creates an instance of an Item, can be of type Weapon,
     *            Armor and Potion that the player may be able to use during 
     *            their journey.
     * @param name the name of the enemy
     * @param minEffect the minimum effect of the item
     * @param maxEffect the maximum effect of the item
     * @param cost the cost of the item
     * @param type the type of the item
     */
    public Item(String name, int minEffect, int maxEffect, int cost, String type)
    {
        this.name = name;
        this.minEffect = minEffect;
        this.maxEffect = maxEffect;
        this.cost = cost;
        this.type = type;
        rand = new Random();
    }

    /**
     * Abstract method declaration
     */
    public abstract void useOn(Entities target);
    public abstract char itemType();
    public abstract int effect();

    /**
     * Function: getMinMaxRand
     * Assertion: Get an integer in the range of min and max.
     * @param min the minimum bound
     * @param max the maximum bound
     * @return the integer retrieved in the range
     */
    protected int getMinMaxRand(int min, int max)
    {
        return rand.nextInt(max + 1 - min) + min;
    }

    /**
     * Function: getName
     * Assertion: Get the name of the item
     * @return the name of the item
     */
    public String getName()
    {
        return name; 
    }

    /**
     * Function: getCost
     * Assertion: Get the cost of the item
     * @return the cost of the item
     */
    public int getCost()
    {
        return cost;
    }

    /**
     * Function: getType
     * Assertion: Get the type of the item
     * @return  the type of the item
     */
    public String getType()
    {
        return type;
    }

    /**
     * Function: getMinEffect
     * Assertion: Get the minimum effect of the item
     * @return the minimun effect of the item
     */
    public int getMinEffect()
    {
        return minEffect;
    }

    /**
     * Function: getMaxEffect
     * Assertion: Get the maximum effect of the item
     * @return the maximum effect of the item
     */
    public int getMaxEffect()
    {
        return maxEffect;
    }
}