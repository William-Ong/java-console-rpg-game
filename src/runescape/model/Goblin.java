/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public class Goblin extends Enemy
{
    private int trigger;

    /**
     * Function: Alternate Constructor of Goblin
     * Assertion: Creates an instance of goblin, a normal enemy spawned
     *            by chance, defeating this enemy will grant player some
     *            rewards.
     * @param name the name of the enemy
     */
    public Goblin(String name) 
    {
        super(name, 30, 3, 4, 8, 8, 20);
        trigger = 0;
    }

    /**
     * Function: specialAbilities
     * Assertion: Each enemy has a unique special abilities that can be
     *            triggered on a fixed change. This mostly alters the 
     *            attack damage with dragon being the exception to be 
     *            able to heal.
     * @param damage the damage to be modified
     * @return the modified damage
     */
    @Override
    protected int specialAbilities(int damage) 
    {
        trigger = 1;
        return damage + 3;
    }

    /**
     * Function: doAttack
     * Assertion: Every entity has their own attack calculation, this will
     *            differ for player and enemy as all enemy has a chance to
     *            trigger their special ability while the player does not.
     * @return the damage to be dealt
     */
    @Override
    public int doAttack()
    {
        int damage = getMinMaxRand(minDamage, maxDamage);
        trigger = 0;
        if(Math.random() < 0.5)
        {
            damage = specialAbilities(damage);
        }
        return damage;
    }

    /**
     * Function: toString
     * Assertion: Returns a string describing the enemy's attack
     * @return the attack description
     */
    @Override
    public String toString()
    {
        return ((trigger == 1) ? "[Goblin use poison arrow!]" : "[Goblin stabs with their dagger!]");
    }

    /**
     * Function: asciiArt
     * Assertion: Display an ASCII art that describes the enemy
     * Reference: https://ascii.co.uk/art/goblin
     * @return the ASCII art formatted to be printed
     */
    @Override
    public String asciiArt()
    {
        return
            "         ,       ,           \n" +
            "        /\\(.-"+"-.)/\\          \n" +
            "    |\\  \\/      \\/  /|      \n" +
            "    | \\ / =.  .= \\ / |      \n" +
            "    \\( \\   o\\/o   / )/      \n" +
            "     \\_, '-/  \\-' ,_/       \n" +
            "       /   \\__/   \\         \n" +
            "       \\ \\__/\\__/ /         \n" +
            "     ___\\ \\|--|/ /___       \n" +
            "   /`    \\      /    `\\     \n" +
            "  /       '----'       \\      ";
    }
}