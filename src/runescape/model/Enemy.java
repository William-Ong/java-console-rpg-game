/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public abstract class Enemy extends Entities
{
    protected abstract int specialAbilities(int damage);

    /**
     * Function: Alternate Constructor of Enemy
     * Assertion: Pass all parameters to the Entity superclass constructor 
     *            as player and enemy are both type Entity
     * @param name the name of the enemy
     * @param health the hitpoints of the enemy
     * @param minDamage the minimum damage the enemy has
     * @param minDefence the minimum defence the enemy has
     * @param maxDamage the maximum damage the enemy has
     * @param maxDefence the maximum defence the enemy has
     * @param gold the amount of gold dropped if the enemy is killed
     */
    public Enemy(String name, int health, int minDamage, int minDefence, int maxDamage, int maxDefence, int gold)
    {
        super(name, health, minDamage, minDefence, maxDamage, maxDefence, gold);
    }

    /**
     * Function: doDefense
     * Assertion: Every entity has their own defense calculation, this
     *            calculates the likelihood in which does the entity
     *            takes or absorbs the damage from an attack.
     * @param damage the damage to be adsorbed
     * @return the defense value calculated
     */
    @Override
    public int doDefense(int damage)
    {
        int defence = getMinMaxRand(minDefence, maxDefence);
        if(defence < damage)
        {
            health -= Math.max(0, damage - defence);
        }
        return defence;
    }

}