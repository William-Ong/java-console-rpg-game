/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

import java.util.Random;

public class FireEnchantment extends WeaponDecorator
{
    private Random rand = new Random();
  
    /**
     * Function: Alternate Constructor of FireConstructor
     * Assertion: One of the four enchantment classes that the shop can offer, this
     *            class boost the player's weapon by a random integer of range 5-10.
     * @param next the item to be enchanted
     */
    public FireEnchantment(Item next)
    {
        super(next);
    }

    /**
     * Function: effect
     * Assertion: Increase the efficiency of the item by a random integer of range 5-10.
     * @return the enchanted efficiency
     */
    @Override
    public int effect()
    {
        // rand turns null after this enchantment is applied, therefore creating new Random resolves that issue
        return next.effect() + rand.nextInt(10 - 5) + 5; //rand(5, 10)
    }

    /**
     * Function: getName
     * Assertion: Return name with modifier
     * @return modified name of item
     */
    @Override
    public String getName()
    {
        return next.getName() + "[Fire Damage]";
    }

    /**
     * Function: getCost
     * Assertion: Return the modified cost of the item
     * @return modified cost of item
     */
    @Override
    public int getCost()
    {
        return next.getCost() + 20;
    }

    /**
     * Function: getMinEffect
     * Assertion: Chain the minimum effect calls upwards to the superclass
     * @return the next minimum effect call
     */
    @Override
    public int getMinEffect()
    {
        return next.getMinEffect();
    }

    /**
     * Function: getMaxEffect
     * Assertion: Chain the maximum effect calls upwards to the superclass
     * @return the next maximum effect call
     */
    @Override
    public int getMaxEffect()
    {
        return next.getMaxEffect();
    }

    /**
     * Function: getType
     * Assertion: Chain the call to get the type of item upwards
     * @return the next type getters call
     */
    @Override
    public String getType()
    {
        return next.getType();
    }
}