/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public class BaseWeapon extends Weapon 
{
    /**
     * Function: Alternate Constructor of BaseWeapon
     * Assertion: Creates an instance of a BaseWeapon, this class does not
     *            have any enchantments, therefore, it is the basic form of 
     *            weapon.
     * @param name the name of the item
     * @param minEffect the minimum effect that the item has
     * @param maxEffect the maximum effect that the item has
     * @param cost the cost of the item
     * @param type the type of the item
     * @param damageType the damage type of the item
     * @param weaponType the weapon type of the item
     */
    public BaseWeapon(String name, int minEffect, int maxEffect, int cost, String type, String damageType, String weaponType)
    {
        super(name, minEffect, maxEffect, cost, type, damageType, weaponType);
    }

    /**
     * Function: effect
     * Assertion: Describes the effect that this item has
     * @return the effect that was calculated
     */
    @Override
    public int effect()
    {
        return super.effect();
    }

    /**
     * Function: getMinEffect
     * Assertion: Getter of the minimum effect of the item
     * @return the minimum effect
     */
    @Override
    public int getMinEffect()
    {
        return super.getMinEffect();
    }

    /**
     * Function: getMaxEffect
     * Assertion: Getter of the maximum effect of the item
     * @return the maximum effect
     */
    @Override
    public int getMaxEffect()
    {
        return super.getMaxEffect();
    }
}