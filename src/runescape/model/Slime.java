/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public class Slime extends Enemy
{
    private int trigger;

    /**
     * Function: Alternate Constructor of Slime
     * Assertion: Creates an instance of slime, a normal enemy spawned
     *            by chance, defeating this enemy will grant player some
     *            rewards.
     * @param name the name of the enemy
     */
    public Slime(String name) 
    {
        super(name, 10, 3, 0, 5, 2, 10);
        trigger = 0;
    }

    /**
     * Function: specialAbilities
     * Assertion: Each enemy has a unique special abilities that can be
     *            triggered on a fixed change. This mostly alters the 
     *            attack damage with dragon being the exception to be 
     *            able to heal.
     * @param damage the damage to be modified
     * @return the modified damage
     */
    @Override
    protected int specialAbilities(int damage) 
    {
        trigger = 1;
        return damage - damage;
    }

    /**
     * Function: doAttack
     * Assertion: Every entity has their own attack calculation, this will
     *            differ for player and enemy as all enemy has a chance to
     *            trigger their special ability while the player does not.
     * @return the damage to be dealt
     */
    @Override
    public int doAttack()
    {
        int damage = getMinMaxRand(minDamage, maxDamage);
        trigger = 0;
        return (Math.random() < 0.2) ? damage : specialAbilities(damage);
    }

    /**
     * Function: toString
     * Assertion: Returns a string describing the enemy's attack
     * @return the attack description
     */
    @Override
    public String toString()
    {
        return ((trigger == 1) ? "[Slime slipped!]" : "[Slime bounces onto you!]");
    }

    /**
     * Function: asciiArt
     * Assertion: Display an ASCII art that describes the enemy
     * Reference: https://textart.sh/topic/slime 
     * @return the ASCII art formatted to be printed
     */
    @Override
    public String asciiArt()
    {
        return
            "        ██████████\n" + 
            "    ██▓▓▒▒▒▒▒▒▒▒▒▒▓▓██\n" +    
            "  ▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓\n" +  
            "  ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██\n" +  
            "██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██\n" +
            "██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██\n" +
            "██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██\n" +
            "  ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██\n" +  
            "    ████▓▓▓▓▓▓▓▓▓▓▓▓██";    
  
    }
}
