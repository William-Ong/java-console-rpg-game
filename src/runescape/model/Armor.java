/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public class Armor extends Item 
{
    private String armorType;

    /**
     * Function: Alternate Constructor of Armor
     * Assertion: Subclass of type Item, the Armor class provides defence capabilities
     *            to the player.
     * @param name the name of the armor
     * @param minEffect the minimum effect of the armor
     * @param maxEffect the maximum effect of the armor
     * @param cost the cost of the armor
     * @param type the type of the armor
     * @param armorType the armor type of the armor
     */
    public Armor(String name, int minEffect, int maxEffect, int cost, String type, String armorType)
    {
        super(name, minEffect, maxEffect, cost, type);
        this.armorType = armorType;
    }

    /**
     * Function: useOn
     * Assertion: An abstract method that is common for all items inheriting Item superclass,
     *            takes in an entity and set new parameters for them
     * @param target the entity to be affected
     */
    @Override
    public void useOn(Entities target)
    {
        target.setMinDefence(minEffect);
        target.setMaxDefence(maxEffect);
    }

    /**
     * Function: getType
     * Assertion: An abstract method that allows the subclass to return a specific 
     *            string that represents their behaviour without knowing that they've
     *            called it.
     * @return the string that contains relevant information regarding the subclass
     */
    @Override
    public String getType()
    {
        return type + " | Armor Type: " + armorType;
    }

    /**
     * Function: itemType
     * Assertion: Returns the item type
     * @return the item type in character format
     */
    @Override
    public char itemType()
    {
        return 'A';
    }

    /**
     * Function: effect
     * Assertion: Describes the effect that this item has
     * @return the effect that was calculated
     */
    @Override
    public int effect()
    {
        return getMinMaxRand(minEffect, maxEffect);
    }

    /**
     * Function: getArmorType
     * Assertion: Known only to the package, gives the armor type
     *            of this item
     * @return the armor type
     */
    protected String getArmorType()
    {
        return armorType;
    }

}