/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public abstract class WeaponDecorator extends Weapon 
{
    protected Item next;

    /**
     * Function: The Decorator Constructor
     * Assertion: Enables the ability to decorate a BaseWeapon to increase
     *            its efficiency and value.
     * @param decoratedWeapon the weapon to be decorated
     */
    public WeaponDecorator(Item decoratedWeapon)
    {
        next = decoratedWeapon;
    }
}

