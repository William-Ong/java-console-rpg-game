/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public class Dragon extends Enemy
{
    private int trigger;
    private double chance;

    /**
     * Function: Alternate Constructor of Dragon
     * Assertion: Creates an instance of dragon, the final boss, defeating
     *            this enemy will automatically end the game.
     * @param name the name of the enemy
     */
    public Dragon(String name) 
    {
        super(name, 100, 15, 15, 30, 20, 100);
        trigger = 0;
    }

    /**
     * Function: specialAbilities
     * Assertion: Each enemy has a unique special abilities that can be
     *            triggered on a fixed change. This mostly alters the 
     *            attack damage with dragon being the exception to be 
     *            able to heal.
     * @param damage the damage to be modified
     * @return the modified damage
     */
    @Override
    protected int specialAbilities(int damage) 
    {
        if(chance < 0.25)
        {
            trigger = 1;
            damage *= 2;
        }
        else if(chance > 0.25 && chance < 0.35)
        {
            trigger = 2;
            health = Math.min(maxHealth, health + 10);
            damage = 0;
        }
        return damage;
    }

    /**
     * Function: doAttack
     * Assertion: Every entity has their own attack calculation, this will
     *            differ for player and enemy as all enemy has a chance to
     *            trigger their special ability while the player does not.
     * @return the damage to be dealt
     */
    @Override
    public int doAttack()
    {
        int damage = getMinMaxRand(minDamage, maxDamage);
        chance = Math.random();
        trigger = 0;
        if(chance < 0.35)
        {
            damage = specialAbilities(damage);
        }
        return damage;
    }

    /**
     * Function: toString
     * Assertion: Returns a string describing the enemy's attack
     * @return the attack description
     */
    @Override
    public String toString()
    {
        return ((trigger == 1) ? "[Dragon use fire breath!]" : (trigger == 2) ? "[Dragon use healing magic!]" : "[Dragon use their claws to slash!]");
    }

    /**
     * Function: asciiArt
     * Assertion: Display an ASCII art that describes the enemy
     * Reference: https://www.asciiart.eu/mythology/dragons 
     * @return the ASCII art formatted to be printed
     */
    @Override
    public String asciiArt()
    {
        return
            "          __                  __\n" +
            "         ( _)                ( _)\n" +
            "        / / \\              / /\\_\\_\n" +
            "       / /   \\            / / | \\ \\\n" +
            "      / /     \\          / /  |\\ \\ \\\n" +
            "     /  /   ,  \\ ,       / /   /|  \\ \\\n" +
            "    /  /    |\\_ /|      / /   / \\   \\_\\\n" +
            "   /  /  |\\/ _ '_| \\   / /   /   \\    \\\\\n" +
            "  |  /   |/  0 \\0\\    / |    |    \\    \\\\\n" +
            "  |    |\\|      \\_\\_ /  /    |     \\    \\\\\n" +
            "  |  | |/    \\.\\ o\\o)  /      \\     |    \\\\\n" +
            "  \\    |     /\\`v-v  /        |    |      \\\\\n" +
            "   | \\/    /_| \\_|  /         |    | \\     \\\\\n" +
            "   | |    /__/_ `-` /   _____  |    |  \\    \\\\\n" +
            "   \\|    [__]  \\_/  |_________  \\   |   \\    ()\n" +
            "    /    [___] (    \\         \\  |\\ |   |   //\n" +
            "   |    [___]                  |\\| \\|   /  |/\n" +
            "  /|    [____]                  \\  |/\\ / / ||\n" +
            " (  \\   [____ /     ) _\\      \\  \\    \\| | ||\n" +
            "  \\  \\  [_____|    / /     __/    \\   / / //\n" +
            "  |   \\ [_____/   / /        \\    |   \\/ //\n" +
            "  |   /  '----|   /=\\____   _/    |   / //\n" +
            " __ /  /        |  /   ___/  _/\\    \\  | ||\n" +
            "(/-(/-\\)       /   \\  (/\\/\\)/  |    /  | /\n" +
            "              (/\\/\\)           /   /   //\n" +
            "                     _________/   /    /\n" +
            "                    \\____________/    (";
    }
}

