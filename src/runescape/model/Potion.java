/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public class Potion extends Item
{
    protected char potionType;

    /**
     * Function: Alternate Constructor of Potion
     * Assertion: Subclass of type Item, the Potion class provides the player
     *            the ability to heal or damage entities.
     * @param name the name of the potion
     * @param minEffect the minimum effect of the potion
     * @param maxEffect the maximum effect of the potion
     * @param cost the cost of the potion
     * @param type the type of the potion
     * @param potionType the potion type
     */
    public Potion(String name, int minEffect, int maxEffect, int cost, String type, char potionType)
    {
        super(name, minEffect, maxEffect, cost, type);
        this.potionType = potionType;
    }

    /**
     * Function: useOn
     * Assertion: An abstract method that is common for all items inheriting Item superclass,
     *            takes in an entity and set new parameters for them
     * @param target the entity to be affected
     */
    @Override
    public void useOn(Entities target)
    {
        if(potionType == 'H' || potionType == 'h')
        {
            target.setHealth(Math.min(target.getMaxHealth(), target.getHealth() + effect()));
        }
        else if(potionType == 'D' || potionType == 'd')
        {
            target.doDefense(effect());
        }
    }

    /**
     * Function: getType
     * Assertion: An abstract method that allows the subclass to return a specific 
     *            string that represents their behaviour without knowing that they've
     *            called it.
     * @return the string that contains relevant information regarding the subclass
     */
    @Override
    public String getType()
    {
        return type + " | Potion Type: " + ((potionType == 'H') ? "Healing" : "Damage");
    }

    /**
     * Function: itemType
     * Assertion: Returns the item type
     * @return the item type in character format
     */
    @Override
    public char itemType()
    {
        return 'P';
    }

    /**
     * Function: effect
     * Assertion: Describes the effect that this item has
     * @return the effect that was calculated
     */
    @Override
    public int effect()
    {
        return getMinMaxRand(minEffect, maxEffect);
    }
    
    /**
     * Function: getPotionType
     * Assertion: Get the potion type of a potion
     * @return the potion type in character format
     */
    protected char getPotionType()
    {
        return potionType;
    }
}