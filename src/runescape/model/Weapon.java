/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public abstract class Weapon extends Item
{
    protected String damageType, weaponType;

    /**
     * Function: Default Constructor of Weapon
     * Assertion: Instantiates all fields and methods of weapon
     */
    public Weapon(){}

    /**
     * Function: Alternate Constructor of Weapon
     * Assertion: Creates an instance of type Weapon, used by the player to
     *            fend off for themselves
     * @param name the name of the weapon
     * @param minEffect the minimum effect of the weapon
     * @param maxEffect the maximum effect of the weapon
     * @param cost the cost of the weapon
     * @param type the type of the weapon
     * @param damageType the damage type of the weapon
     * @param weaponType the weapon type
     */
    public Weapon(String name, int minEffect, int maxEffect, int cost, String type, String damageType, String weaponType)
    {
        super(name, minEffect, maxEffect, cost, type);
        this.damageType = damageType;
        this.weaponType = weaponType;
    }

    /**
     * Function: useOn
     * Assertion: An abstract method that is common for all items inheriting Item superclass,
     *            takes in an entity and set new parameters for them
     * @param target the entity to be affected
     */
    @Override
    public void useOn(Entities target)
    {
        target.setMinDamage(minEffect);
        target.setMaxDamage(maxEffect);
    }

    /**
     * Function: getType
     * Assertion: An abstract method that allows the subclass to return a specific 
     *            string that represents their behaviour without knowing that they've
     *            called it.
     * @return the string that contains relevant information regarding the subclass
     */
    @Override
    public String getType()
    {
        return type + " | Damage Type: " + damageType + " | Weapon Type: " + weaponType;
    }

     /**
     * Function: itemType
     * Assertion: Returns the item type
     * @return the item type in character format
     */
    @Override
    public char itemType()
    {
        return 'W';
    }

    
    /**
     * Function: effect
     * Assertion: Describes the effect that this item has
     * @return the effect that was calculated
     */
    @Override
    public int effect()
    {
        return getMinMaxRand(minEffect, maxEffect);
    }
    
    /**
     * Function: getDamageType
     * Assertion: Get the damage type of the weapon
     * @return the damage type
     */
    public String getDamageType()
    {
        return damageType;
    }

    /**
     * Function: getWeaponType
     * Assertion: Get the weapon type of the weaopn
     * @return the weapon type
     */
    public String getWeaponType()
    {
        return weaponType;
    }
}