/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

import java.util.Random;

public abstract class Entities
{
    protected String name;
    protected int health;
    protected int maxHealth;
    protected int minDamage;
    protected int minDefence;
    protected int maxDamage;
    protected int maxDefence;
    protected int gold;
    protected Random rand;

    /**
     * Abstract method declaration
     */
    public abstract int doAttack();
    public abstract int doDefense(int damage);
    public abstract String toString();
    public abstract String asciiArt();

    /**
     * Function: Alternate Constructor of Entites
     * Assertion: This class contains all the relevant information regarding the
     *            overall existance of an entity.
     * @param name the name of the entity
     * @param health the hitpoints of the entity
     * @param minDamage the minimum damage that the entity has
     * @param minDefence the minimum defence that the entity has
     * @param maxDamage the maximum damage that the entity has
     * @param maxDefence the maximum defence that the entity has
     * @param gold the value of the entity
     */
    public Entities(String name, int health, int minDamage, int minDefence, int maxDamage, int maxDefence, int gold)
    {
        this.name = name;
        this.health = health;
        this.maxHealth = health;
        this.minDamage = minDamage;
        this.minDefence = minDefence;
        this.maxDamage = maxDamage;
        this.maxDefence = maxDefence;
        this.gold = gold;

        rand = new Random();
    }

    /**
     * Function: getMinMaxRand
     * Assertion: Get an integer in the range of min and max.
     * @param min the minimum bound
     * @param max the maximum bound
     * @return the integer retrieved in the range
     */
    protected int getMinMaxRand(int min, int max)
    {
        return rand.nextInt(max + 1 - min) + min;
    }

    /**
     * Function: getName
     * Assertion: Get the name of the entity
     * @return the name of the entity
     */
    public String getName() 
    {
        return this.name;
    }

    /**
     * Function: setName
     * Assertion: Set the new name for the entity
     * @param name the new name
     */
    public void setName(String name) 
    {
        this.name = name;
    }

    /**
     * Function: getHealth
     * Assertion: Get the hitpoints of the entity
     * @return the hitpoints
     */
    public int getHealth() 
    {
        return this.health;
    }

    /**
     * Function: setHealth
     * Assertion: Set the new hitpoints of the entity
     * @param health the new hitpoints
     */
    public void setHealth(int health) 
    {
        this.health = health;
    }

    /**
     * Function: getMaxHealth
     * Assertion: Get the maximum hitpoints of an entity
     * @return the max health
     */
    public int getMaxHealth()
    {
        return maxHealth;
    }

    /**
     * Function: getMinDamage
     * Assertion: Get the minimum damage that the entity can deal
     * @return the minimum damage
     */
    public int getMinDamage() 
    {
        return this.minDamage;
    }

    /**
     * Function: setMinDamage
     * Assertion: Set the new minimum damage for the entity
     * @param minDamage the new minimum damage
     */
    public void setMinDamage(int minDamage) 
    {
        this.minDamage = minDamage;
    }

    /**
     * Function: getMinDefence
     * Assertion: Get the minimum defence that the entity has
     * @return the minimum defence
     */
    public int getMinDefence() 
    {
        return this.minDefence;
    }

    /**
     * Function: setMinDefence
     * Assertion: Set the new minimum defence for the entity
     * @param minDefence the new minimum defence
     */
    public void setMinDefence(int minDefence) 
    {
        this.minDefence = minDefence;
    }

    /**
     * Function: getMaxDamage
     * Assertion: Get the maximum damage that the entity can deal
     * @return the maximum damage
     */
    public int getMaxDamage() 
    {
        return this.maxDamage;
    }

    /**
     * Function: setMaxDamage
     * Assertion: Set the new maximum damage for the entity
     * @param maxDamage the new maximum damage
     */
    public void setMaxDamage(int maxDamage) 
    {
        this.maxDamage = maxDamage;
    }

    /**
     * Function: getMaxDefence
     * Assertion: Get the maximum defence that the entity has
     * @return the maximum defence
     */
    public int getMaxDefence() 
    {
        return this.maxDefence;
    }

    /**
     * Function: setMaxDefence
     * Assertion: Set the new maximum defence for the entity
     * @param maxDefence the new maximum defence
     */
    public void setMaxDefence(int maxDefence) 
    {
        this.maxDefence = maxDefence;
    }

    /**
     * Function: getGold
     * Assertion: Get the value of the entity
     * @return the gold value
     */
    public int getGold() 
    {
        return this.gold;
    }

    /**
     * Function: setGold
     * Assertion: Set the new value of the entity
     * @param gold the new gold value
     */
    public void setGold(int gold) 
    {
        this.gold = gold;
    }

    /**
     * Function: alive
     * Assertion: A simple boolean check to see if the entity is still alive
     * @return the boolean check
     */
    public boolean alive()
    {
        return health > 0;
    }
}