/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public class DamageTwoEnchantment extends WeaponDecorator
{
    /**
     * Function: Alternate Constructor of DamageTwoEnchantment
     * Assertion: One of the four enchantment classes that the shop can offer, this
     *            class boost the player's weapon by 2 damage.
     * @param next the item to be enchanted
     */
    public DamageTwoEnchantment(Item next)
    {
        super(next);
    }

    /**
     * Function: effect
     * Assertion: Increase the efficiency of the item by 2.
     * @return the enchanted efficiency
     */
    @Override
    public int effect()
    {
        return next.effect() + 2;
    }

    /**
     * Function: getName
     * Assertion: Return name with modifier
     * @return modified name of item
     */
    @Override
    public String getName()
    {
        return next.getName() + "[+2]";
    }
    
    /**
     * Function: getCost
     * Assertion: Return the modified cost of the item
     * @return modified cost of item
     */
    @Override
    public int getCost()
    {
        return next.getCost() + 5;
    }

    /**
     * Function: getMinEffect
     * Assertion: Chain the minimum effect calls upwards to the superclass
     * @return the next minimum effect call
     */
    @Override
    public int getMinEffect()
    {
        return next.getMinEffect();
    }

    /**
     * Function: getMaxEffect
     * Assertion: Chain the maximum effect calls upwards to the superclass
     * @return the next maximum effect call
     */
    @Override
    public int getMaxEffect()
    {
        return next.getMaxEffect();
    }

    /**
     * Function: getType
     * Assertion: Chain the call to get the type of item upwards
     * @return the next type getters call
     */
    @Override
    public String getType()
    {
        return next.getType();
    }
}