/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

import java.util.LinkedList;
import java.util.List;

public class Shop 
{
    private List<Item> items;

    /**
     * Function: Default Constructor of Shop
     * Assertion: This model class holds all the item that the
     *            player can buy from.
     */
    public Shop()
    {
        items = new LinkedList<>();
    }

    /**
     * Function: addItem
     * Assertion: Adds item to the shop itself.
     * @param item the item to be added
     */
    public void addItem(Item item)
    {
        items.add(item);
    }

    /**
     * Function: getItem
     * Assertion: Get the references to an item in the shop
     * @param index the index to retrieve the reference
     * @return the reference itself
     */
    public Item getItem(int index)
    {
        return items.get(index);
    }

    /**
     * Function: getList
     * Assertion: Get the entire list of the shop
     * @return the list of items
     */
    public List<Item> getList()
    {
        return items;
    }

    /**
     * Function: getSize
     * Assertion: Get the size of the shop item list
     * @return the size of the item list
     */
    public int getSize()
    {
        return items.size();
    }
}