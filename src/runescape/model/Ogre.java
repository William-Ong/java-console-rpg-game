/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.model;

public class Ogre extends Enemy
{
    private int trigger;

    /**
     * Function: Alternate Constructor of Ogre
     * Assertion: Creates an instance of ogre, a normal enemy spawned
     *            by chance, defeating this enemy will grant player some
     *            rewards.
     * @param name the name of the enemy
     */
    public Ogre(String name) 
    {
        super(name, 40, 5, 6, 10, 12, 40);
        trigger = 0;
    }

    /**
     * Function: specialAbilities
     * Assertion: Each enemy has a unique special abilities that can be
     *            triggered on a fixed change. This mostly alters the 
     *            attack damage with dragon being the exception to be 
     *            able to heal.
     * @param damage the damage to be modified
     * @return the modified damage
     */
    @Override
    protected int specialAbilities(int damage) 
    {
        trigger = 1;
        return damage + getMinMaxRand(minDamage, maxDamage);
    }

    /**
     * Function: doAttack
     * Assertion: Every entity has their own attack calculation, this will
     *            differ for player and enemy as all enemy has a chance to
     *            trigger their special ability while the player does not.
     * @return the damage to be dealt
     */
    @Override
    public int doAttack()
    {
        int damage = getMinMaxRand(minDamage, maxDamage);
        trigger = 0;
        if(Math.random() < 0.2)
        {
            damage = specialAbilities(damage);
        }
        return damage;
    }
    
    /**
     * Function: toString
     * Assertion: Returns a string describing the enemy's attack
     * @return the attack description
     */
    @Override
    public String toString()
    {
        return ((trigger == 1) ? "[Ogre use ground stomp to stun and attacks you with their warhammer!]" : "[Ogre use it's mighty warhammer to swing at you!]");
    }

    /**
     * Function: asciiArt
     * Assertion: Display an ASCII art that describes the enemy
     * Reference: https://ascii.co.uk/art/ogre
     * @return the ASCII art formatted to be printed
     */
    @Override
    public String asciiArt()
    {
        return
            "        __,='`````'=/\n" +
            "        '//  (o) \\(o) \\ `'         _,-,\n" +
            "        //|     ,_)   (`\\      ,-'`_,-\\" + "\n" +
            "      ,-~~~\\  `'==='  /-,      \\==```` \\__\n" +
            "     /        `----'     `\\     \\       \\/\n" +
            "  ,-`                  ,   \\  ,.-\\       \\" + "\n" +
            " /      ,               \\,-`\\`_,-`\\_,..--'\\" + "\n" +
            ",`    ,/,              ,>,   )     \\--`````\\" + "\n" +
            "(      `\\`---'`  `-,-'`_,<   \\      \\_,.--'`\n" +
            " `.      `--. _,-'`_,-`  |    \\" + "\n" +
            "  [`-.___   <`_,-'`------(    /\n" +
            "  (`` _,-\\   \\ --`````````|--`\n" +
            "   >-`_,-`\\,-` ,          |\n" +
            " <`_,'     ,  /\\          /\n" + 
            "  `  \\/\\,-/ `/  \\/`\\_/V\\_/\n" +
            "     (  ._. )    ( .__. )\n" +
            "     |      |    |      |\n" +
            "      \\,---_|    |_---./\n" +
            "      ooOO(_)    (_)OOoo";
    }
}