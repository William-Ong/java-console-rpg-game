/**
 * Name: MING HANG ONG
 * ID: 19287368
 * Unit: OOSE - Object-Oriented Software Engineering
 */
package runescape.view;

import java.util.Scanner;

import runescape.controller.FileLoader;
import runescape.controller.LoaderException;
import runescape.controller.LoaderFactory;
import runescape.model.Entities;
import runescape.model.Player;

public class UserInterface 
{
    private static Scanner input = new Scanner (System.in);

    private LoaderFactory loaderFactory;

    /**
     * Function: Alternate Constructor of UserInterface
     * Assertion: This class handles all user input and output.
     * @param loaderFactory the loader factory
     */
    public UserInterface (LoaderFactory loaderFactory)
    {
        this.loaderFactory = loaderFactory;
    } 

    /**
     * Function: showLoadOption
     * Assertion: Show the loading option that the player can choose to load their
     *            files from.
     * @return Player's choice (integer)
     */
    public int showLoadOption()
    {
        return intInput("[Load file?]\n(0) Confirm\n(1) Exit\nEnter option: ");
    }

    /**
     * Function: showMenu
     * Assertion: Show game menu to the user
     * @return Player's choice (integer)
     */
    public int showMenu(Player player)
    {   
        return intInput("[Select an action.]\n(1) Go to Shop\n(2) " + (player == null ? "Create a character" : "Change character name") 
                        + "\n(3) Choose Weapon\n(4) Choose Armour\n(5) Start Battle\n(6) Exit\nEnter option: ");
    }

    /**
     * Function: showBattleMenu
     * Assertion: During battle, the player will be able to select a couple of actions.
     * @return Player's choice (integer)
     */
    public int showBattleMenu()
    {
        return intInput("[Select an action.]\n(1) Attack\n(2) Use Potions\n(3) Check Statistic\n(4) Retreat\nEnter option: ");        
    }

    /**
     * Function: showShopOption
     * Assertion: This allows the user to buy or sell items.
     * @return Player's choice (integer)
     */
    public int showShopOption()
    {
        return intInput("[Select an action.]\n(1) Buy\n(2) Sell\n(3) Load new items\nEnter option: ");
    }

    /**
     * Function: showEnchantmentOption
     * Assertion: Show which equipment the player can apply the enchantment to
     * @return Player's choice (integer)
     */
    public int showEnchantmentOption()
    {
        return intInput("[Select which to enchant.]\n(1) Equipped Weapon\n(2) Check Inventory\nEnter option: ");
    }

    /**
     * Function: invalidOption
     * Assertion: Display a simple error message.
     */
    public void invalidOption()
    {
        display("[Invalid option! Try again.]");
    }

    /**
     * Function: intInput
     * Assertion: Handles user integer inputs 
     * @param prompt the prompt to be printed out to the user
     * @return received user integer inputs
     */
    public int intInput(String prompt)
    {
        System.out.print(prompt);
        return Integer.parseInt(input.nextLine());
    }

    /** 
     * Function: strInput
     * Assertion: Handles user string inputs 
     * @param prompt the prompt to be printed out to the user
     * @return received user string inputs
     */
    public String strInput(String prompt)
    {
        System.out.print(prompt);
        return input.nextLine();
    }

    /** 
     * Function: display
     * Assertion: Displays message
     * @param prompt the prompt to be printed out to the user
     */
    public void display(String prompt)
    {
        System.out.println(prompt);
    }

    /**
     * Function: playerTurn
     * Assertion: During battle, the player must select one of the choices
     *            (except checking statistic) to end their turn.
     * @param player the Player object
     * @return Player's choice (integer)
     */
    public int playerTurn(Player player)
    {
        int choice = 0;
        boolean finished = false;
        do
        {
            try
            {
                choice = intInput("[Select an action.]\n(1) Attack\n(2) Use Potions\n(3) Check Statistic\n(4) Retreat\nSelect an option: ");
                if(choice == 1 || choice == 2 || choice == 4)
                {
                    finished = true;
                }
                else if(choice == 3)
                {
                    display(player.attribute());
                }
                else
                {
                    display("[Invalid option! Try again.]");
                }
            }
            catch (NumberFormatException e)
            {
                finished = false;
                display(e.getMessage());
            }
        }while(!finished);
        return choice;
    }

    /**
     * Function: getFileLoader
     * Assertion: Get a specific file loader to load from
     *            different sources.
     * @param loaders the map of loaders
     * @return the loader being used
     */
    public FileLoader getFileLoader(String file) throws LoaderException
    {
        FileLoader loader = null;
        boolean finished = false;
        int choice = 0;
        do
        {
            try
            {
                choice = showLoadOption();
                if(choice == 0)
                {
                    finished = true;
                    loader = loaderFactory.makeLoader(file);
                }
                else if(choice == 1)
                {
                    throw new IllegalArgumentException("[Exiting...]");
                }
                else
                {
                    invalidOption();
                }
            }
            catch(IllegalArgumentException e)
            {
                throw new LoaderException(e.getMessage(), e);
            }
        }while(!finished);
        return loader;
    }

    /**
     * Function: displayHP
     * Assertion: Display the current hitpoint of an entity
     * @param entity the entity to check
     */
    public void displayHP(Entities entity)
    {
        display("[" + entity.getName() + "'s HP: " + (entity.getHealth() < 0 ? 0 : entity.getHealth()) + "]");
    }

    /**
     * Function: displayDamageDone
     * Assertion: Display the damage done from one entity to another
     * @param entityDealt the entity dealing damage
     * @param entityReceived the entity receiving damage
     * @param damage the damage dealt
     */
    public void displayDamageDone(Entities entityDealt, Entities entityReceived, int damage)
    {
        display("[" + entityDealt.getName() + " dealt " + damage + " damage to " + entityReceived.getName() + "!]");
    }

    /**
     * Function: displayDefence
     * Assertion: Display the defence of the defending entity
     * @param entity the entity defending
     * @param defence the entity's defence 
     */
    public void displayDefence(Entities entity, int defence)
    {
        display("[" + entity.getName() + " has " + defence + " defence!]");
    }
}