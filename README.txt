OOSE Assignment - Turn-based RPG Game
By Ong Ming Hang (19287368)

.
├── DeclarationOfOriginality_v12.pdf          | The declaration of originality provide from Blackboard
├── OOSE Report.pdf							  | The report
├── OOSE UML.png						      | The UML Diagram for the program
├── README.txt								  | README file providing instructions to build and run the program
├── build								      | The directory containing all the files generated from compiler
│   └── runescape						      | .
│       ├── controller						  | .
│       │   ├── BattleEngine.class			  | .
│       │   ├── EntityFactory.class			  | .
│       │   ├── FileLoader.class			  | .
│       │   ├── GameEngine.class			  | .
│       │   ├── ItemFactory.class			  | .
│       │   ├── LoadByCSV.class				  | .
│       │   ├── LoaderException.class		  | .
│       │   ├── LoaderFactory.class			  | .
│       │   ├── Main.class					  | .
│       │   └── ShopEngine.class			  | .
│       ├── model							  | .
│       │   ├── Armor.class				      | .
│       │   ├── BaseWeapon.class			  | .
│       │   ├── DamageFiveEnchantment.class	  | .
│       │   ├── DamageTwoEnchantment.class    | .
│       │   ├── Dragon.class				  | .
│       │   ├── Enemy.class					  | .
│       │   ├── Entities.class				  | .
│       │   ├── FireEnchantment.class		  | .
│       │   ├── Goblin.class				  | .
│       │   ├── Item.class					  | .
│       │   ├── Ogre.class					  | .
│       │   ├── Player.class				  | .
│       │   ├── Potion.class				  | .
│       │   ├── PowerEnchantment.class		  | .
│       │   ├── Shop.class					  | .
│       │   ├── Slime.class					  | .
│       │   ├── Weapon.class				  | .
│       │   └── WeaponDecorator.class		  | .
│       └── view							  | .
│           └── UserInterface.class			  | .
├── build.xml								  | The file that builds the program using 'ant'
├── dist									  | The directory containing the fully compiled program and its test file
│   ├── DownMarketRunescape.jar				  | Program in .jar format
│   └── shop_input.csv						  | Shop input test file provided from the specification
├── resources								  | The directory containing the test file to be copied during build stage
│   └── shop_input.csv						  | The test file
└── src										  | The directory containing all of the source file
    └── runescape							  | The main directory
        ├── controller						  | The controller package
        │   ├── BattleEngine.java			  | Controls the battle phase of the game
        │   ├── EntityFactory.java			  | Creates entity that represents a player or enemy
        │   ├── FileLoader.java				  | Interface for extensible file loading 
        │   ├── GameEngine.java				  | Main controller that acts as the brain
        │   ├── ItemFactory.java			  | Creates items to be added into the shop or enchants weapon
        │   ├── LoadByCSV.java				  | Load comma-seperated-values file contents
        │   ├── LoaderException.java		  | Custom exception
        │   ├── LoaderFactory.java			  | Creates loader based on file format
        │   ├── Main.java					  | Instantiates main controllers and launch the game
        │   └── ShopEngine.java				  | Controls the shop functionality
        ├── model							  | The directory containing all models
        │   ├── Armor.java					  | Player-exclusive items that adds defence capabilities to them
        │   ├── BaseWeapon.java				  | A basic weapon without any form of enchantment
        │   ├── DamageFiveEnchantment.java	  | An enchantment that adds +5 to the weapon's damage
        │   ├── DamageTwoEnchantment.java	  | An enchantment that adds +2 to the weapon's damage
        │   ├── Dragon.java					  | The final boss of the game
        │   ├── Enemy.java					  | An enemy superclass that allows extensibility when creating new enemies
        │   ├── Entities.java				  | An entity superclass that creates different types of entity
        │   ├── FireEnchantment.java          | An enchantment that adds a random chance between 5-10 to the weapon's damage
        │   ├── Goblin.java					  | One of the non-dragon enemies
        │   ├── Item.java					  | The item superclass that adds behaviour to all types of items
        │   ├── Ogre.java					  | One of the non-dragon enemies
        │   ├── Player.java					  | The actual player that the user controls during the program
        │   ├── Potion.java					  | A potion that can either heal or damage entities
        │   ├── PowerEnchantment.java		  | An enchantment that adds a 1.1x miltiplier to the weapon's damage
        │   ├── Shop.java					  | Contains field that stores item to be sold to player
        │   ├── Slime.java					  | One of the non-dragon enemies
        │   ├── Weapon.java					  | Player-exclusive items that adds attack capabilities to them
        │   └── WeaponDecorator.java		  | The decorator class that allows weapons to be enchanted
        └── view							  | The directory containing the view class
            └── UserInterface.java			  | The view class that handles all of the user I/O

To build the program:
$ cd Assignment/
$ ant

To run and use the program/
$ cd Assignment/dist/
$ java -jar DownMarketRunescape.jar
